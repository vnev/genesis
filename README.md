To run, type `npm install` followed by `npm start`.


There are currently 3 routes:
`/`: This GET route is just a basic message route that just shows Hello World.
Doesn't do much more.


`/connect`: This GET route needs to be called the first time in order to download
the blockchain and merkle tree JSON files.


`/transact`: This POST route accepts some data in the form of a Javascript dict.
It adds the data to the blockchain and updates the merkle tree.
