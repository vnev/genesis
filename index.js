let app 		= require("express")();
let bodyParser 	= require("body-parser");
let Blockchain 	= require("./blockchain");
let Block 		= require("./block");
var fs			= require("fs");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const BLOCKCHAIN_DIR = "./blockchain";
let blockchain = new Blockchain();

// error check middleware
var checkBlockchainDirExists = (req, res, next) => {
	if (req.path === "/" || req.path === "/connect") {
		next();
	}
	else if (!fs.existsSync(BLOCKCHAIN_DIR)) {
		console.log("Blockchain directory does not exist. GET /connect first to establish first time connection");
		res.status(400).json({ message: "Operation not permitted" });
	}
	next();
}

app.use(checkBlockchainDirExists);

app.get("/", (req, res) => {
	res.status(200).json({ message: "Hello world" });
});

app.get("/connect", (req, res) => {
	// create blockchain directory
	// this route can double as some kind of authentication for the user
	if (!fs.existsSync(BLOCKCHAIN_DIR)) {
		fs.mkdirSync(BLOCKCHAIN_DIR);
	}

	writeBlockchainFile();
	blockchain.rebuildMerkleTree();

	// TODO: download entire blockchain into BLOCKCHAIN_DIR as JSON file
	return res.status(200).json({ message: "Successfully connected" });
});

function writeBlockchainFile() {
	fs.writeFile((BLOCKCHAIN_DIR + "/blockchain.json"), JSON.stringify(blockchain.chain), (err) => {
		if (err) {
			return res.status(500).json({ message: "Internal server error" });
		}

		console.log("Wrote blockchain JSON file");
	});
}

app.post("/transact", (req, res) => {
	let data = req.body.data;
	/*  data represents something new that is
		to be added to the blockchain. In the case
		of a supply chain application, this would be
		details of a particular order/sale
	*/
	// currently a basic transaction implementation
	let newBlock = new Block(new Date(), data);
	blockchain.addBlock(newBlock);
	writeBlockchainFile();
	blockchain.rebuildMerkleTree();
	console.log(JSON.stringify(newBlock));

	res.status(200).json({ message: "Transaction completed." });
});

var port = process.env.PORT || 8000;
app.listen(port, () => {
	console.log("Listening on port " + port);
});
