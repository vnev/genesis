let merkle = require("merkle", false);
let Block = require("./block");
let fs = require("fs");

const BLOCKCHAIN_DIR = "./blockchain";

class Blockchain {
	constructor() {
		this.chain = [this.getGenesisBlock()];
		// merkle("sha256").async(this.chain, function (err, tree) {
		// 	if (err) {
		// 		console.log("Could not make merkle tree. " + err);
		// 		exit(1);
		// 	}
		// 	console.log("===========\nDEBUG\n===========");
		// 	console.log("Root: " + tree.root());
		// 	console.log("level 0: " + tree.level(0) + "\nlevel 1: " + tree.level(1));
		// });

		// for now we can probably just call rebuildMerkleTree
		// to generate the new merkle tree
		this.rebuildMerkleTree();
	}

	getGenesisBlock() {
		let genesisBlock = new Block(new Date(), "Genesis Block");
		genesisBlock.idx = 0;
		genesisBlock.previousHash = "0";
		return genesisBlock;
	}

	latestBlock() {
		return this.chain[this.chain.length - 1];
	}

	rebuildMerkleTree() {
		var t = merkle("sha256").async(this.chain, function (err, tree) {
			if (err) {
				console.log("Could not make merkle tree. " + err);
				exit(1);
			}
			console.log("===========\nDEBUG\n===========");
			console.log("Root: " + tree.root());
			console.log("Level 0: " + tree.level(0) + "\nLevel 1: " + tree.level(1));
			
			// basic structure to be written to the file
			// TODO: write the full merkle tree complete
			// with hashes, etc.
			var treeObj = {
				root: tree.root(),
				level: tree.level(),
				depth: tree.depth(),
				levels: tree.levels(),
				nodes: tree.nodes()
			};

			fs.writeFile((BLOCKCHAIN_DIR + "/merkleTree.json"), JSON.stringify(treeObj), (err) => {
				if (err) {
					return res.status(500).json({ message: "Internal server error" });
				}

				console.log("Wrote to merkleTree.json");
			});
		});
	}

	addBlock(blockToAdd) {
		// push block to merkle tree
		blockToAdd.idx = ++this.latestBlock().idx;
		blockToAdd.previousHash = this.latestBlock().hash;
		blockToAdd.hash = blockToAdd.getHash();
		this.chain.push(blockToAdd);
		this.rebuildMerkleTree();
	}

	checkValid() {
		// validate merkle tree
	}
}

// some tester code
// var tree = new Blockchain();
// tree.addBlock(new Block(new Date(), { data: "Some cool data", nonce: 2 }));

module.exports = Blockchain;