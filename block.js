let sha256 = require("crypto-js/sha256");

class Block {
	constructor(ts, data) {
		this.idx = 0; // index
		this.ts = ts; // timestamp
		this.data = data; // data
		this.previousHash = 0; // pointer to prev hash
		this.nonce = 0; // nonce
		this.hash = this.getHash(); // hash for current block
	}

	getHash() {
		// use sha256 or md5 to generate hash on given data
		// not sure if we need this when we're using a merkle tree
		let toHash = this.idx + this.previousHash + this.ts + this.data + this.nonce;
		return sha256(toHash).toString();
	}
}

module.exports = Block;